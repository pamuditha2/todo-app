export interface Response {
  status: number;
  msg?: string;
  data?: any;
  error?: any;
  success: boolean;
}
