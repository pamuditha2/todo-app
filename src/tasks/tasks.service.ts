import { Task } from './entities/task.entity';
import { Response } from '../types/response';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TasksService {
  constructor(@InjectModel('Task') private readonly taskModel: Model<any>) {}

  async create(createTaskDto: CreateTaskDto): Promise<Response> {
    const task = new this.taskModel(createTaskDto);
    const saved: Task = await task.save();
    return {
      status: 201,
      msg: 'Task Successfully Created',
      data: saved,
      success: true,
    };
  }

  async findAll(): Promise<Response> {
    const tasks: Task[] = await this.taskModel.find();
    if (tasks.length === 0)
      return {
        status: 404,
        msg: 'No Tasks Found',
        success: true,
      };
    return {
      status: 200,
      data: tasks,
      success: true,
    };
  }

  async findOne(id: string): Promise<Response> {
    const task: Task = await this.taskModel.findById(id);
    if (!task)
      return {
        status: 404,
        msg: 'Task Not Found',
        success: false,
      };

    return {
      status: 200,
      data: task,
      success: true,
    };
  }

  async update(id: string, updateTaskDto: UpdateTaskDto) {
    updateTaskDto.modifiedAt = new Date();

    const task = await this.taskModel.findOneAndUpdate(
      { _id: id },
      updateTaskDto,
      { new: true },
    );
    if (!task)
      return {
        status: 404,
        msg: 'Task Not Found',
        success: false,
      };

    return {
      status: 200,
      data: task,
      success: true,
    };
  }

  async remove(id: string): Promise<Response> {
    const task = await this.taskModel.deleteOne({ _id: id });
    if (task.deletedCount === 0)
      return {
        status: 400,
        msg: 'Task Not Found',
        success: false,
      };

    return {
      status: 200,
      msg: 'Task Successfully Removed',
      data: task,
      success: true,
    };
  }
}
