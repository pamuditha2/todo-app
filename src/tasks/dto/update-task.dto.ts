import { PartialType } from '@nestjs/mapped-types';
import { CreateTaskDto } from './create-task.dto';

import {
  IsBoolean,
  IsDate,
  IsString,
  Length,
  MinLength,
  IsNotEmpty,
} from 'class-validator';
export class UpdateTaskDto extends PartialType(CreateTaskDto) {
  @IsNotEmpty()
  @IsString()
  @Length(3, 20)
  readonly title: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  readonly description: string;

  @IsNotEmpty()
  @IsBoolean()
  readonly completed: boolean;

  @IsNotEmpty()
  @IsDate()
  readonly createdAt: string;

  @IsNotEmpty()
  @IsDate()
  modifiedAt: any;
}
