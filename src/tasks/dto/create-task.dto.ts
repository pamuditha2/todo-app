import { IsString, Length, MinLength, IsNotEmpty } from 'class-validator';

export class CreateTaskDto {
  @IsNotEmpty()
  @IsString()
  @Length(3, 20)
  readonly title: string;

  @IsString()
  @MinLength(3)
  readonly description: string;
}
